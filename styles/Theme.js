const mainColor = "#4CBCE3";
const mainColorDark = "#b52c22";
const secondaryColor = "#e84d1c";
const silver = "#C0C0C0";
const lightSilver = "#F2F2F2";
const grey = "#808080";
const darkGrey = "#585858";
const black = "#000000";


// app font

const boldFont = "Montserrat-Bold";
const mediumFont = "Montserrat-Medium";
const normalFont = "Montserrat-Regular";
const italicFont = "Montserrat-Italic";

const libelSuitRg = "Libel Suite";
const LibreFranklinBold = "LibreFranklin-Bold";
const LibreFranklinLight = "LibreFranklin-Light";
const LibreFranklinLightItalic = "LibreFranklin-LightItalic";
const LibreFranklinMedium = "LibreFranklin-Medium";
const LibreFranklinRegular = "LibreFranklin-Regular";
const LibreFranklinSemiBold = "LibreFranklin-SemiBold";

const webservice_main_link = "https://www.voy.com.py/apiV2/";


export default {
  webservice_main_link,
  libelSuitRg,
  LibreFranklinBold,
  LibreFranklinLight,
  LibreFranklinLightItalic,
  LibreFranklinMedium,
  LibreFranklinRegular,
  LibreFranklinSemiBold,
  mainColor,
  secondaryColor,
  boldFont,
  mainColorDark,
  mediumFont,
  normalFont,
  italicFont,
  silver,
  grey,
  darkGrey,
  lightSilver,
  black,
}

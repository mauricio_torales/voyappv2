import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import Theme from '../styles/Theme';

const screenWidth = Math.round(Dimensions.get('window').width);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 10,
        // marginLeft: 16,
        // marginRight: 16,
        marginTop: 15,
        // marginBottom: 8,
        // borderRadius: 5,
        // backgroundColor: '#FFF',
        // elevation: 2,
    },
    photo: {
        height: 200,
        width: '80%',
    },
    title: {
        fontSize: 20,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#000',
    },
});
function renderText(ultimo_dia){
    if(ultimo_dia==''){
      return <Text></Text>
    }{
      return <Text><Text style={{  color: Theme.silver }}>AL</Text>{ultimo_dia}</Text>
    }
  }

function goToDetails(props, totalItemList, item) {
    props.navigation.navigate("Sexto", { id: item.id, totalItemList: totalItemList, categories: getSimilarEventList(item, totalItemList) });
}

function getSimilarEventList(itemParam, totalItemList) {
    let dataList = [];

    const itemData = totalItemList.map((item, key) => {
        {
            item.categoria == itemParam.categoria && item.id != itemParam.id
                ? dataList.push(item)
                : "";
        }
    });
    return dataList;
};

const EventListRow = ({ data, props, totalItemList, itemData }) => (
    <View style={styles.container}>
    
     <View style={{ flexDirection: 'column' }}>

        <Text style={{ fontSize: 30,  marginLeft: 15, marginTop: 5, marginBottom: 5 }}>{itemData.toUpperCase()}</Text>
            <View style={{ flexDirection: 'row', width: '100%' }}>
            
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                                   <View style={{ flexDirection: 'row' }}>


                {data.map((item, key) => (
                    <TouchableWithoutFeedback onPress={() => goToDetails(props, totalItemList, item)}>
                        
                        <View style={{ marginLeft: 15, marginRight: 15, marginTop: 0 }}>
                            <Image
                                source={{ uri: item.vistaprevia }}
                                style={{ width:'100%',height:400,resizeMode:'contain' }}
                            />
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    marginTop: 0,
                                    width: screenWidth -50,
                                }}>
                                <Text
                                    style={{
                                        color: Theme.darkGrey, fontWeight: '200', fontSize: 18,
                                        width: '50%', textAlign: 'left'
                                    }}
                                    onPress={() => {

                                    }}>
                                    {item.titulo.toUpperCase()}
                                    {"\n"}
                                    <Text style={{  color: Theme.mainColor }} onPress={() => goToDetails(props, totalItemList, item)}>
                                        Ver Más....
                                    </Text>
                                </Text>
                                <Text style={{ color: Theme.darkGrey, fontSize: 25,  width: '50%', textAlign: 'right' }}>{item.dia_evento}{renderText(item.ultimo_dia)}<Text style={{  color: Theme.silver }}>{item.mes_evento.substring(0, 3).toUpperCase()}</Text >
                                </Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                ))}
                  </View>
            </ScrollView>
        </View>
        {/* <Image source={image_url} style={styles.photo} /> */}
        {/* <Text style={styles.title}>
            {title}
        </Text> */}
    </View>
    </View>
);

export default EventListRow;
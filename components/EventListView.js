import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import EventListRow from './EventListRow';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          marginTop:5,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };
function getEventList(itemParams, totalItemList) {
    let dataList = [];

    const itemData = totalItemList.map((item, key) => {
        {
            // item.dia_evento + item.mes_evento + item.año_evento == itemParams
            //     ? dataList.push(item)
            //     : "";
            console.log(item.mes_evento);
              item.mes_evento == itemParams
                ? dataList.push(item)
                : "";
            
        }
    });
    console.log(dataList.length);
    return dataList;
};

const EventListView = ({ itemList, totalItemList, props }) => (
    <View style={styles.container}>
        <FlatList
            ItemSeparatorComponent={renderSeparator}
            data={itemList}
            renderItem={({ item }) => <EventListRow
                props={props}
                //data={item}
                data={getEventList(item, totalItemList)}
                totalItemList={totalItemList}
                itemData={item}
                // title={item.title}
                // description={item.text}
                // image_url={item.uri}
            />}
        />

    </View>
);

export default EventListView;
import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import Theme from '../styles/Theme';

const screenWidth = Math.round(Dimensions.get('window').width);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
    },
    photo: {
        height: 200,
        width: '80%',
    },
    title: {
        fontSize: 20,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#000',
    },
});

function goToDetails(props, id) {
    props.navigation.navigate("Sexto", { id: id, p_event: 'p_event' });
}

const PermanentEventListRow = ({ data, props, itemData }) => (

    <View style={styles.container}>
        <View style={{ flexDirection: 'column' }}>
            {/* <Text style={{ fontSize: 30, fontFamily: Theme.LibreFranklinBold, marginLeft: 15, marginTop: 5, marginBottom: 5 }}>{itemData}</Text> */}
            <View style={{ flexDirection: 'row', width: '100%' }}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>
                    <View style={{ flexDirection: 'row',marginTop:0 }}>
                        {data.map((item, key) => (
                            <TouchableWithoutFeedback onPress={() => goToDetails(props, item.id)}>
                                <View style={{ marginLeft: 15, marginRight: 15 }}>
                                    <Image
                                        source={{ uri: item.vistaprevia }}
                                        style={{ width: '100%', height: 400 ,resizeMode:'contain' }}
                                    />
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginTop:0,
                                            width: screenWidth -35,
                                        }}>
                                        <Text
                                            style={{ color: Theme.darkGrey, fontWeight: '200', fontSize:18, textAlign: 'left' }}
                                            onPress={() => {

                                            }}>
                                            {item.titulo.toUpperCase()}
                                            {"\n"}
                                            <Text style={{ color: Theme.mainColor }} onPress={() => goToDetails(props, item.id)}>
                                                Ver Más...
                                            </Text>
                                        </Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        ))}
                    </View>
                </ScrollView>
            </View>
        </View>
    </View>
);

export default PermanentEventListRow;
import React from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import PlacesListRow from './PlacesListRow';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          marginTop:5,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };
function getPlacesEventList(itemParams, totalItemList) {
    let dataList = [];

    const itemData = totalItemList.map((item, key) => {
        {
            item.lugar == itemParams
                ? dataList.push(item)
                : "";
        }
    });
    return dataList;
};

const PlacesListView = ({ itemList, totalItemList, props }) => (
    <View style={styles.container}>
        <FlatList
            ItemSeparatorComponent={renderSeparator}
            data={itemList}
            renderItem={({ item }) => <PlacesListRow
                props={props}
                data={getPlacesEventList(item, totalItemList)}
                itemData={item}
                totalItemList={totalItemList}
                />}
        />

    </View>
);

export default PlacesListView;
import React from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import PermanentEventListRow from './PermanentEventListRow';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          marginTop:5,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };
function getPlacesEventList(itemParams, totalItemList) {
    let dataList = [];

    const itemData = totalItemList.map((item, key) => {
        {
            item.titulo == itemParams
                ? dataList.push(item)
                : "";
        }
    });
    return dataList;
};

const PermanentEventListView = ({ itemList, totalItemList, props }) => (
    <View style={styles.container}>
        <FlatList
            ItemSeparatorComponent={renderSeparator}
            data={itemList}
            renderItem={({ item }) => <PermanentEventListRow
                props={props}
                data={getPlacesEventList(item, totalItemList)}
                itemData={item}
            />}
        />

    </View>
);

export default PermanentEventListView;
import React from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import Theme from '../styles/Theme';

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
    },
    photo: {
        height: 200,
        width: '80%',
    },
    title: {
        fontSize: 20,
        marginTop: 5,
        fontWeight: 'bold',
        color: '#000',
    },
});
const renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          marginTop:5,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };
  function renderText(ultimo_dia){
    if(ultimo_dia==''){
      return <Text></Text>
    }{
      return <Text><Text style={{  color: Theme.silver,marginLeft:5,marginRight:5 }}>AL</Text>{ultimo_dia}</Text>
    }
  };
function goToDetails(props, totalItemList, item) {
    props.navigation.navigate("Sexto", { id: item.id, totalItemList: totalItemList, categories: getSimilarEventList(item, totalItemList) });
}

function getSimilarEventList(itemParam, totalItemList) {
    let dataList = [];

    const itemData = totalItemList.map((item, key) => {
        {
            item.categoria == itemParam.categoria && item.id != itemParam.id
                ? dataList.push(item)
                : "";
        }
    });
    return dataList;
};

const PlacesListRow = ({ data, props, totalItemList, itemData }) => (

    <View style={styles.container}>
        <View style={{ flexDirection: 'column',paddingBottom:30 }}>
            <Text style={{ fontSize: 30,  marginLeft: 15, marginTop:5, marginBottom:10 }}>{itemData.toUpperCase()}</Text>
            <View style={{ flexDirection: 'row', width: '100%' }}>
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>
                    <View style={{ flexDirection: 'row',marginTop:-25 }}>
                        {data.map((item, key) => (
                            <TouchableWithoutFeedback onPress={() => goToDetails(props, totalItemList, item)}>
                                <View style={{ marginLeft: 15, marginRight: 15 }}>
                                    <Image
                                        source={{ uri: item.vistaprevia }}
                                        style={{ width: '100%', height: 400,resizeMode:'contain'  }}
                                    />
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            marginTop:0,
                                            width: screenWidth -35,
                                        }}>
                                        <Text
                                            style={{ color: Theme.darkGrey, fontWeight: '200', fontSize:18,  width: '50%', textAlign: 'left' }}
                                            onPress={() => {

                                            }}>
                                            {item.titulo.toUpperCase()}
                                            {"\n"}
                                            <Text style={{ color: Theme.mainColor }} onPress={() => goToDetails(props, totalItemList, item)}>
                                                Ver Más...
                                            </Text>
                                        </Text>
                                        <Text style={{ color: Theme.darkGrey, fontSize: 25,  width: '50%', textAlign: 'right' }}>
                                        {item.dia_evento}{renderText(item.ultimo_dia)}<Text style={{  color: Theme.silver,marginLeft:5 }}>{item.mes_evento.substring(0, 3).toUpperCase()}</Text>
                                        </Text>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        ))}
                    </View>
                </ScrollView>
            </View>
        </View>
    </View>
);

export default PlacesListRow;
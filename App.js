//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
import { connect } from 'react-redux';
//import react in our code.
import * as Font from 'expo-font';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
  Platform,
  Text,
} from 'react-native';
// import all basic components
 
//For React Navigation 3+
//import {
//  createStackNavigator,
//  createDrawerNavigator,
//  createAppContainer,
//} from 'react-navigation';
 
//For React Navigation 4+
import {createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Inicio from './pages/Inicio';
import Eventos from './pages/Eventos';
import Lugares from './pages/Lugares';
import Permanentes from './pages/Permanentes';
import Contacto from './pages/Contacto';
import Evento from './pages/Evento';
import Prueba from './pages/Prueba';

import Menu from './Menu';
 
class NavigationDrawerStructure extends Component {
  //Structure for the navigatin Drawer
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('./img/drawer.png')}
            style={{ width: 25, height: 25, marginLeft: 5 }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
 
const FirstActivity_StackNavigator = createStackNavigator({
  //All the screen from the Screen1 will be indexed here
  First: {
    screen: Inicio,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
    }),
  },
});

const Screen2_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Second: {
    screen: Eventos,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
const Screen3_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Third: {
    screen: Lugares,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
const Screen4_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Fourth: {
    screen: Permanentes,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
const Screen5_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Quinto: {
    screen: Contacto,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
const Screen6_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Sexto: {
    screen: Evento,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
const Screen7_StackNavigator = createStackNavigator({
  //All the screen from the Screen2 will be indexed here
  Prueba: {
    screen: Prueba,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />
    }),
  },
});
 
//Drawer Navigator Which will provide the structure of our App
const DrawerNavigatorExample = createDrawerNavigator(
  {
    //Drawer Optons and indexing
    Inicio: {
      screen: FirstActivity_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Inicio',
      },
    },
    Eventos: {
      screen: Screen2_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Eventos',
      },
    },
    Lugares: {
      screen: Screen3_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Lugares',
      },
    },
    Permanentes: {
      screen: Screen4_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Permanentes',
      },
    },
    Contacto: {
      screen: Screen5_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Contacto',
      },
    },
    Evento: {
      screen: Screen6_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Evento',
      },
    },
    Prueba: {
      screen: Screen7_StackNavigator,
      navigationOptions: {
        drawerLabel: 'Prueba',
      },
    },
  },
  {
    //For the Custom sidebar menu we have to provide our CustomSidebarMenu
    contentComponent: Menu,
    //Sidebar width
    drawerWidth: Dimensions.get('window').width - 100,
  }
);
export default createAppContainer(DrawerNavigatorExample);
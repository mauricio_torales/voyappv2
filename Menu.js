//This is an example code for Navigation Drawer with Custom Side bar//
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text,  Linking } from 'react-native';
import { Icon } from 'react-native-elements';
 
export default class CustomSidebarMenu extends Component {
  constructor() {
    super();
    //Setting up the Main Top Large Image of the Custom Sidebar
    this.proileImage =
      'https://www.voy.com.py/img/barro.png';
    //Array of the sidebar navigation option with icon and screen to navigate
    //This screens can be any screen defined in Drawer Navigator in App.js
    //You can find the Icons from here https://material.io/tools/icons/
    this.items = [
      {
        navOptionThumb: 'home',
        navOptionName: 'Inicio',
        screenToNavigate: 'Inicio',
      },
      {
        navOptionThumb: 'stars',
        navOptionName: 'Eventos',
        screenToNavigate: 'Eventos',
      },
      {
        navOptionThumb: 'place',
        navOptionName: 'Lugares',
        screenToNavigate: 'Lugares',
      },
      {
        navOptionThumb: 'account-balance',
        navOptionName: 'Actividades Permanentes',
        screenToNavigate: 'Permanentes',
      },
      {
        navOptionThumb: 'local-post-office',
        navOptionName: 'Contacto',
        screenToNavigate: 'Contacto',
      },
     
    ];
  }
  render() {
    return (
      <View style={styles.sideMenuContainer}>
        {/*Top Large Image */}
        <Image
          source={{ uri: this.proileImage }}
          style={styles.sideMenuProfileIcon}
        />
        {/*Divider between Top Image and Sidebar Option*/}
        <View
          style={{
            width: '100%',
            height: 1,
            backgroundColor: '#e2e2e2',
            marginTop:-28,
          }}
        />
        {/*Setting up Navigation Options from option array using loop*/}
        <View style={{ width: '100%' }}>
          {this.items.map((item, key) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: global.currentScreenIndex === key ? '#fff' : '#ffffff',
              }}
              key={key}>
              <View style={{ marginRight: 10, marginLeft: 20 }}>
                <Icon name={item.navOptionThumb} size={20} color="#808080" />
              </View>
              <Text
                style={{
                  fontSize: 18,
                  color: global.currentScreenIndex === key ? '#4160AA' : 'black',
                }}
                onPress={() => {
                  global.currentScreenIndex = key;
                  this.props.navigation.navigate(item.screenToNavigate);
                }}>
                {item.navOptionName}
              </Text>
            </View>
          ))}
        </View>
        <View style={styles.footerContainer}>
          <Text style={{fontSize:20}} onPress={() => {
            //on clicking we are going to open the URL using Linking
            Linking.openURL('https://linco.com.py/');
          }}>Linco</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 11 ,
  },
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: '100%',
    height: 150,
    marginTop:5,
  },
  footerContainer: {
    alignItems: 'center',
    position:'absolute',
    left:0,
    bottom:0,
    right:0,
    padding:15,
    width:'100%',
    marginTop:'100%',
    backgroundColor: 'lightgrey',
    opacity:0.5,
  },
});
//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { WebView,ScrollView,Dimensions,TextInput, StatusBar,SafeAreaView, StyleSheet, View, Image, Text } from "react-native";
import { Button } from 'react-native-elements';
import Theme from "../styles/Theme";
import { sendEmail } from '../utils/SendMail';
const ancho = Math.round(Dimensions.get('window').width);

export default class Contacto extends Component {
  static navigationOptions = {  
    headerTitle: (
          <View style={{flex:1,}}>
              <Image
                  source={require('../img/logo.png') }
                   style={{marginLeft:60,width:120, height:40,marginTop:-10}}
              />
          </View>
      ),
      headerStyle: {  
          backgroundColor: '#ffffff',  
      },  
  };
  constructor(props) {
    super(props);
    this.state = {
        number: '',
        email: '',
        telephone: '',
        message: '',
    };
}
getInitialState() {
    return {
        region: {
            latitude: -25.2795144,
            longitude: -57.5686088,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
    };
}

onRegionChange(region) {
    this.setState({ region });
}
componentDidMount = () => { };

sendEmail() {
    if (this.state.message == '') return alert('Debe ingresar un mensaje')

    sendEmail(
        'mauricio@linco.com.py',
        'Query',
        this.state.message
    ).then(() => {
        console.log('Our email successful provided to device mail ');
    });
}


  render() {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
        <View style={{ flext: 1, padding: 0, marginTop:0 }}>
            <ScrollView>
            <Image source={require('../img/header-contacto.png') }
                    style={styles.imagenCabecera}/>
                <StatusBar backgroundColor="blue" />

                <View style={{ marginLeft: 15 }}>

                    <Text style={{ fontSize: 24, marginTop:10 ,marginLeft: 15, color: Theme.black }}>CONTACTO</Text>
                    <TextInput style={{  borderBottomWidth: 1, marginRight: 5,  height:50,fontSize: 16, }}
                        placeholder={'Nombre'}
                        placeholderTextColor = "#000000"
                        onChangeText={(text) => this.state.number = text}
                    />
                    <TextInput style={{  borderBottomWidth: 1, marginRight: 5 , height:50 ,fontSize: 16,}}
                        placeholder={'Email'}
                        placeholderTextColor = "#000000"
                        onChangeText={(text) => this.state.email = text}
                    />
                    <TextInput style={{  borderBottomWidth: 1, marginRight: 5 , height:50 ,fontSize: 16,}}
                        placeholder={'Teléfono'}
                        placeholderTextColor = "#000000"
                        onChangeText={(text) => this.state.telephone = text}
                    />
                    <TextInput style={{  lineHeight: 30, borderBottomWidth: 1, marginRight: 5 , height:50,fontSize: 16, }}
                        multiline={true}
                        numberOfLines={4}
                        placeholder={'Mensaje'}
                        placeholderTextColor = "#000000"
                        onChangeText={(text) => this.state.message = text}
                    />
                    <Button
                        buttonStyle={styles.button}
                        type="outline"
                        titleStyle={{ color: Theme.black ,fontSize: 16,fontWeight: 'bold' }}
                        title="ENVIAR"
                        onPress={() => this.sendEmail()}
                    />

                    <View style={{ marginTop: 20, flexDirection: 'row' }}>
                        <View style={{ width: '50%' }}>
                            <Text style={{  color: Theme.black,fontSize: 16,fontWeight: 'bold'  }}>Teléfono:</Text>
                            <Text style={{ color: Theme.black,fontSize: 16  }}>0981 400774</Text>
                        </View>
                        <View style={{ width: '50%' }}>
                            <Text style={{  color: Theme.black,fontSize: 16 ,fontWeight: 'bold' }}>Direccion:</Text>
                            <Text style={{  color: Theme.black ,fontSize: 16 }}>López Moreira c/ R. Colmán Asunción </Text>
                        </View>
                    </View>
                </View>
                <View style={{ width: ancho, height: 300, marginTop: 20 }}>
                    <WebView
                        source = {{ uri:
                        'https://www.voy.com.py/contacto/mapa.html'}}
                        />  
                        
                </View>

              
            </ScrollView>
        </View>
        </SafeAreaView>
    );
}
}


const styles = StyleSheet.create({
branchContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    marginTop: 50,
    justifyContent: "center"
},
button: {
    width: "40%",
    // height: 40,
    padding: 10,
    borderColor: '#000',
    marginTop: 10,
    backgroundColor: 'white',
},
map: {
    height: 400,
    marginTop: 80
},
imagenCabecera:{
    width:ancho,
    height:200,
  }
});

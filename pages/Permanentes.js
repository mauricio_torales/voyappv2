//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { ActivityIndicator, StatusBar,SafeAreaView, StyleSheet, View, Image, Text,TouchableWithoutFeedback } from "react-native";
import { SearchBar } from 'react-native-elements';
import PermanentEventListView from '../components/PermanentEventListView';
import Theme from "../styles/Theme";
export default class Permanentes extends Component {
  static navigationOptions = {  
    headerTitle: (
          <View style={{flex:1,}}>
              <Image
                  source={require('../img/logo.png') }
                  style={{marginLeft:60,width:120, height:40,marginTop:-15}}
              />
          </View>
      ),
      headerStyle: {  
          backgroundColor: '#ffffff',  
      },  
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      search: '',
      events: [],
      totalEvents: [],
      pages: 0,
      currentPage: 1,
    };
    this.eventsHolder = [];
    this.totalEventsHolder = [];
  };

  componentDidMount = () => {
    this.getEventList();
  };

  getEventList() {
    console.log("getEventList");

    fetch(Theme.webservice_main_link+'paginas/')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          pages: responseJson.paginas,
        }, function () {

        });
      })
      .catch((error) => {
        console.error(error);
      }).then(() => {
        fetch(Theme.webservice_main_link+'actividades/?page=' + this.state.currentPage)
          .then((response) => response.json())
          .then((responseJson) => {
            let array = responseJson.eventos

            const eventsMap = Object.assign({}, ...array.map(s => ({ [s.titulo]: s.titulo })));
            const eventsArray = Object.values(eventsMap)

            this.setState({
              isLoading: false,
              events: eventsArray,
              totalEvents: responseJson.eventos,
            }, function () {
              this.eventsHolder = eventsArray;
              this.totalEventsHolder = responseJson.eventos;

            });
          }).done();
      }).done();

  };

  getNextEventList(param) {
    if (param == 1 && this.state.currentPage != this.state.pages) {
      this.setState({
        isLoading: true,
        currentPage: this.state.currentPage + 1,
      })
    } else if (param == 0 && this.state.currentPage != 0) {
      this.setState({
        isLoading: true,
        currentPage: this.state.currentPage - 1,
      })
    } else {
      return;
    }
    console.log("asdasdasdsadasdasdsd");
    fetch(Theme.webservice_main_link+'actividades/?page=' + this.state.currentPage)
      .then((response) => response.json())
      .then((responseJson) => {

        let array = responseJson.eventos

        const eventsMap = Object.assign({}, ...array.map(s => ({ [s.titulo]: s.titulo })));
        const eventsArray = Object.values(eventsMap)

        this.setState({
          isLoading: false,
          events: eventsArray,
          totalEvents: responseJson.eventos,
        }, function () {
          this.eventsHolder = eventsArray;
          this.totalEventsHolder = responseJson.eventos;

        });
      })
      .catch((error) => {
        console.error(error);
      });
  }


  search = text => {
  };

  clear = () => {
    this.search.clear();
  };

  searchFilterFunction(text) {
    //passing the inserted text in textinput
    let newData = this.totalEventsHolder.filter(function (item) {
      //applying filter for the inserted text in search bar
      const itemData = item.titulo ? item.titulo.toUpperCase() : ''.toUpperCase()
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    let array = newData

    const placesMap = Object.assign({}, ...array.map(s => ({ [s.titulo]: s.titulo })));
    const placesArray = Object.values(placesMap)

    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      events: placesArray,
      search: text,
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20, justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#BD1624" />
        </View>
      )
    }

    const { search } = this.state.events;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

      <View>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <View style={{ width: "100%", height: "100%" }}>
          <SearchBar
             round
             searchIcon={{ size: 24,color:'#000' }}
             containerStyle={{ backgroundColor: "#FFFFFF" }}
             onChangeText={text => this.searchFilterFunction(text)}
             onClear={text => this.searchFilterFunction('')}
             placeholder="Buscar actividad..."
             value={this.state.search}
             placeholderTextColor={'#000000'}
             lightTheme
          />
          <PermanentEventListView
            itemList={this.state.events}
            totalItemList={this.state.totalEvents}
            props={this.props}
          />


          <View style={{alignItems:'center'}}>

              <View style={{ marginTop:10,marginBottom:10, flexDirection: 'row' }}>
                {this.state.currentPage != 1
                  ?
                  <TouchableWithoutFeedback onPress={() => this.getNextEventList(0)}>
                  <Image 
                        source= {require("../img/ant.png")}
                        style={{ width: 35, height: 30,marginRight:20,resizeMode:'contain'}}/>

                  </TouchableWithoutFeedback>
                  :
                  <Text></Text>
                
                  }
                {this.state.currentPage != this.state.pages
                  ?
                  <TouchableWithoutFeedback onPress={() => this.getNextEventList(1)}>

                    <Image 

                        source= {require("../img/sig.png")}
                        style={{ width: 35,  height: 30 ,marginLeft:20,resizeMode:'contain'}}/>
                </TouchableWithoutFeedback>

                :
                <Text></Text>
                  
                }

              </View>
          </View>
        </View>
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  branchContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    marginTop: 50,
    justifyContent: "center"
  }
});

//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { ActivityIndicator, Dimensions, Image, ScrollView,SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View,  } from 'react-native';
// import all basic components
import Theme from "../styles/Theme";
import * as Font from 'expo-font';
const ancho = Math.round(Dimensions.get('window').width);
 
export default class Inicio extends React.Component {
    static navigationOptions = {  
        headerTitle: (
            <View style={{flex:1,}}>
                <Image
                     source={require('../img/logo.png') }
                    style={{marginLeft:60,width:120, height:40,marginTop:-15}}
                />
            </View>
        ),
        headerStyle: {  
            backgroundColor: '#fff',  
        },  
    };
    constructor(props) {
        super(props);
    
        this.state = {
          position: 1,
          interval: null,
          dataSource: [
          ],
          currentMonth:"",
          isLoading: true,
          eventDataSource: [],
          featureEventDataSource: [],
        };
      }
      componentWillMount() {
        this.setState({
          interval: setInterval(() => {
            this.setState({
              position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
            });
          }, 4000)
        });
      }
      componentWillUnmount() {
        clearInterval(this.state.interval);
      }
        componentDidMount() {

          Font.loadAsync({
            'Mansalva': require('../assets/fonts/man.ttf'),
          });
  
        var today = new Date();
        monthName='';
        monthday = parseInt(today.getMonth()+1);
        if(monthday==1){
          monthName='enero';
        }
        else if(monthday==2){
          monthName='febrero';
        }
        else if(monthday==3){
          monthName='marzo';
        }
        else if(monthday==4){
          monthName='abril';
        }
        else if(monthday==5){
          monthName='mayo';
        }
        else if(monthday==6){
          monthName='junio';
        }
        else if(monthday==7){
          monthName='julio';
        }
        else if(monthday==8){
          monthName='agosto';
        }
        else if(monthday==9){
          monthName='septiembre';
        }
        else if(monthday==10){
          monthName='octubre';
        }
        else if(monthday==11){
          monthName='noviembre';
        }
        else if(monthday==12){
          monthName='diciembre';
        }
        this.setState({
          currentMonth: monthName,
        });
    
    
        this.getEventDetails();
        // this.getFeaturedEvents();
      }
      getEventDetails() {
        fetch(Theme.webservice_main_link+'inicio')
          .then((response) => response.json())
          .then((responseJson) => {
    
            if('error' in responseJson) {
            }
            else {
            let slideImages = [];
            const test = responseJson.eventos.map((item) => {
              let map = { "url": item.vistaprevia }
              slideImages.push(map);
            }
            );
            this.setState({
              eventDataSource: responseJson.eventos,
            });
          }
    
          })
          .catch((error) => {
            console.error(error);
          }).then(() => {
            fetch(Theme.webservice_main_link+'destacados')
              .then((response) => response.json())
              .then((responseJson) => {
    
                this.setState({
                  isLoading: false,
                  featureEventDataSource: responseJson.eventos,
                }, function () {
    
                });
              }).done();
          }).done();;
      }
      getFeaturedEvents() {
        fetch(Theme.webservice_main_link+'destacados')
          .then((response) => response.json())
          .then((responseJson) => {
    
            this.setState({
              isLoading: false,
              featureEventDataSource: responseJson.eventos,
            }, function () {
    
            });
          })
          .catch((error) => {
            console.error(error);
          });
      }
      goToDetails(id) {

        this.props.navigation.navigate("Sexto", { id: id });
      }
      renderText(ultimo_dia){
        if(ultimo_dia==''){
          return <Text></Text>
        }{
          return <Text><Text style={{  color: Theme.silver,}}>AL</Text>{ultimo_dia}</Text>
        }
      }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1 }}>
          <Image source={require('../img/header-inicio.png') }
                    style={styles.imagenCabecera}/>
              <Text style={{fontSize: 30, marginLeft: 15, marginTop: 10}}> {this.state.currentMonth.toUpperCase()}</Text>
          <ActivityIndicator size="large" color="#BD1624" style={{padding: 20, justifyContent: 'center'}} />
        </View>
      )
    }
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

      <View style={styles.container}>
        <ScrollView>
          <View style={styles.MainContainer}>
            <Image source={require('../img/header-inicio.png') }
                    style={styles.imagenCabecera}/>
              <Text style={{fontSize: 30, marginLeft: 15, marginTop: 10}}> {this.state.currentMonth.toUpperCase()}</Text>
          </View>
          <View style={{ flexDirection: 'row', width: '100%', marginTop:0 }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {this.state.eventDataSource.map((item) => (

                <TouchableWithoutFeedback onPress={() => this.goToDetails(item.id)} >
                  <View style={{ marginLeft: 15, marginRight: 15 }} >
                    <Image
                      source={{ uri: item.vistaprevia }}
                      style={{ width: '100%', height: 400 ,resizeMode:'contain'}}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop:0,
                        width: ancho -50,
                      }}>
                      <Text
                        style={{ color: Theme.darkGrey,  fontSize: 18,fontWeight: '200', width: '50%', textAlign: 'left' }}
                        onPress={() => {

                        }}>
                        {item.titulo.toUpperCase()}
                        {"\n"}
                        <Text style={{  color: Theme.mainColor }} onPress={() => this.goToDetails(item.id)}>
                          Ver más...
                        </Text>
                      </Text>
                      <Text style={{ color: Theme.darkGrey, fontSize: 25,  width: '50%', textAlign: 'right' }}>
                      {item.dia_evento}{this.renderText(item.ultimo_dia)}<Text style={{  color: Theme.silver,marginLeft:10 }}>{item.mes_evento.substring(0, 3).toUpperCase()}</Text>
                      </Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              ))}
            </ScrollView>
          </View>

          <View
        style={{
          height: 1,
          width: "100%",
          marginTop:20,
          backgroundColor: "#CED0CE",
        }}
      />
          <Text style={{fontSize: 24,  marginLeft: 15, marginTop: 10}}> {'EVENTOS DESTACADOS'.toUpperCase()} </Text>
          <View style={{ flexDirection: 'row', width: '100%', marginBottom: 20, marginTop:0 }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {this.state.featureEventDataSource.map((item) => (
                <TouchableWithoutFeedback onPress={() => this.goToDetails(item.id)}>
                  <View style={{ marginLeft: 15, marginRight: 15 }}>
                    <Image
                      source={{ uri: item.vistaprevia }}
                      style={{ width: '100%', height: 400,resizeMode:'contain' }}
                    />
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop:0,
                        width: ancho -50,
                      }}>
                      <Text
                        style={{ color: Theme.darkGrey, fontWeight: '200', fontSize: 18, width: '50%', textAlign: 'left' }}
                        onPress={() => {

                        }}>
                        {item.titulo.toUpperCase()}
                        {"\n"}
                        <Text style={{  color: Theme.mainColor }} onPress={() => this.goToDetails(item.id)}>
                          Ver más...
                      </Text>
                      </Text>
                      <Text style={{ color: Theme.darkGrey, fontSize: 25,  width: '50%', textAlign: 'right' }}>
                      {item.dia_evento}{this.renderText(item.ultimo_dia)}<Text style={{  color: Theme.silver }}>{item.mes_evento.substring(0, 3).toUpperCase()}</Text>
                      </Text>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              ))}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  imagenCabecera:{
    width:ancho,
    height:200,
  }
});
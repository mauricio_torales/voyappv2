import React from "react";
import { WebView,ActivityIndicator, Dimensions, Image,Linking, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Theme from "../styles/Theme";
import Slideshow from 'react-native-slideshow';


const screenWidth = Math.round(Dimensions.get('window').width);

class EventDetailsScreen extends React.Component {
    static navigationOptions = {  
        headerTitle: (
              <View style={{flex:1,}}>
                  <Image
                      source={require('../img/logo.png') }
                       style={{marginLeft:60,width:120, height:40,marginTop:-10}}
                  />
              </View>
          ),
          headerStyle: {  
              backgroundColor: '#ffffff',  
          },  
      };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            eventDetails: {},
            categories: [],
            dataSource:[
                {
                    key: '',
                    text: 'https://www.senatur.gov.py/',
                    title: '',
                    url: require("../img/publi-2.png"),
                  },
                  {
                    key: '',
                    text: 'https://oiledesign.com/',
                    title: '',
                    url: require("../img/publi-1.png"),
                  },
                  {
                      key: '',
                      text: 'https://www.senatur.gov.py/',
                      title: '',
                      url: require("../img/publi-4.png"),
                    },
                    {
                      key: '',
                      text: 'https://oiledesign.com/',
                      title: '',
                      url: require("../img/publi-3.png"),
                    },
              ],
            position: 0,
        };
    }
    componentWillMount() {
        newPostion=0;
        this.setState({
            interval: setInterval(() => {
                 newPostion=this.state.position+1;
                if(newPostion>=this.state.dataSource.length){
                    newPostion=0                
                }
              this.setState({   
                position: newPostion,
              });
            }, 4000)
          });
      }
      componentWillUnmount() {
        clearInterval(this.state.interval);
      }
    
    componentDidMount() {
        this.getEventDetails();
    }
    getSimilarEventList(itemParam) {
        itemParam.map((item, key) => {
            {
                itemParam = item;
            }
        });
        let dataList = [];
        let totalItemList = this.props.navigation.getParam('totalItemList', []);
        const itemData = totalItemList.map((item, key) => {
            {
                item.categoria == itemParam.categoria && item.id != itemParam.id
                    ? dataList.push(item)
                    : "";
            }
        });
        return dataList;
    };

    seliderLinkopen(position) {
        console.log(position);
        url=this.state.dataSource[position].text;
        console.log(url);
        Linking.openURL(url).catch(err => console.error('An error occurred', err));


    }
    getSimilarEventDetails(id) {
        fetch(Theme.webservice_main_link+'eventos/?id=' + id)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.evento);
                this.setState({
                    isLoading: false,
                    eventDetails: responseJson.evento,
                    categories: this.getSimilarEventList(responseJson.evento),
                }, function () {

                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    getEventDetails() {
        let url = Theme.webservice_main_link+'/eventos/?id=';
        if (this.props.navigation.getParam('p_event') == 'p_event') {
            url = Theme.webservice_main_link+'actividades/?id=';
        }
        fetch(url + this.props.navigation.getParam('id', 0))
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson.evento);
                this.setState({
                    isLoading: false,
                    eventDetails: responseJson.evento,
                    categories: this.props.navigation.getParam('categories', []),
                }, function () {

                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, padding: 20, justifyContent: 'center' }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }

        const eventDetails = this.state.eventDetails;

        return (
            <View style={{ flext: 1 }}>
                <ScrollView>
                    <StatusBar backgroundColor="blue" />

                    <Image
                        source={{ uri: eventDetails[0].portada }}
                        style={{ width: "100%", height: 400 ,resizeMode:'contain' }}
                    />
                    <View style={{ margin: 10, backgroundColor: Theme.lightSilver }}>
                        <Text style={{ fontSize: 24,  marginLeft: 15, marginTop: 10 }}>{eventDetails[0].titulo.toUpperCase()}</Text>
                        <Text style={{  fontSize: 15, marginLeft: 15, marginTop: 10 }}>{eventDetails[0].descripcion}</Text>
                       

                        <Text style={{  fontSize: 15, marginLeft: 15, marginTop: 20 }}>
                        <Text style={{  }}>Dirección: </Text>{eventDetails[0].direccion}</Text>
                        {
                            (this.props.navigation.getParam('p_event') == 'p_event')
                                ?
                                <Text style={{  fontSize: 15, marginLeft: 15, marginTop: 2 }}>
                                    <Text style={{  }}>Lugar: </Text>{eventDetails[0].lugar}</Text>
                                :
                                <Text style={{  fontSize: 15, marginLeft: 15, marginTop: 2 }}>
                                    <Text style={{  }}>Fecha: </Text>
                                    Desde el {eventDetails[0].dia_evento} {eventDetails[0].mes_evento} {eventDetails[0].año_evento} {"\n"}
                                    Hasta el {eventDetails[0].ultimo_dia} {eventDetails[0].ultimo_mes} {eventDetails[0].ultimo_año}
                                </Text>
                        }

                        <Text style={{  fontSize: 15, marginLeft: 15, marginTop: 2 }}>

                        <Text style={{  }}>Horario: </Text>{eventDetails[0].hora}</Text>
                        {/* <Text style={{ fontSize: 15, marginLeft: 15, marginTop: 2 }}>{eventDetails[0].titulo}</Text> */}
                        {/* <Text style={{ fontSize: 15, marginLeft: 15, marginTop: 2, marginBottom: 10 }}>{eventDetails[0].titulo}</Text> */}

                        {/* <Button
                            buttonStyle={styles.button}
                        // title="Entrada: "
                        >Entrada: {eventDetails[0].entrada}</Button> */}

                        <TouchableOpacity style={styles.buttonOther} activeOpacity={.5}>
                            <Text style={{  color: 'white' }} > Entrada: {eventDetails[0].entrada} </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ width: screenWidth, height: 300 }}>
                    <WebView
                        source = {{ uri:
                        'https://www.voy.com.py/evento/mapa.php?id=' +  eventDetails[0].id }}
                        />  
                    </View>
                   <View style={{marginTop:20}}>
                   <Slideshow
                        height={300}
                        indicatorSize={0}
                        arrowSize={0}
                        resizeMode="stretch"
                        onPress={() => this.seliderLinkopen(this.state.position)}
                        dataSource={this.state.dataSource}
                        position={this.state.position}
                        onPositionChanged={position => this.setState({ position })} />
                   </View>

                    {this.state.categories.length != 0 ?
                        <Text style={{ fontSize: 24,  marginLeft: 15, marginTop: 20 }}>  {'Eventos relacionados'.toUpperCase()}</Text> :
                        <Text style={styles.title}></Text>}

                    <View style={{ marginTop:10, flexDirection: 'row', width: '100%' }}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            {this.state.categories.map((item, key) => (
                                <TouchableOpacity onPress={() => this.getSimilarEventDetails(item.id)}>
                                    <View style={{ marginLeft: 15, marginRight: 15 }}>
                                        <Image
                                            source={{ uri: item.vistaprevia }}
                                            style={{ width: screenWidth - 35, height: 300 ,resizeMode:'contain'}}
                                        />
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                                marginTop: 5,
                                                width: screenWidth - 35,
                                            }}>
                                            <Text
                                                style={{ color: Theme.darkGrey, fontWeight: '200', fontSize: 18, width: '50%', textAlign: 'left' }}
                                                onPress={() => {

                                                }}>
                                                {item.titulo.toUpperCase()}
                                                {"\n"}
                                                <Text style={{  color: Theme.mainColor }} onPress={() => this.getSimilarEventDetails(item.id)}>
                                                    Ver Más...
                                                </Text>
                                            </Text>
                                            <Text style={{ color: Theme.darkGrey, fontSize: 25,  width: '50%', textAlign: 'right' }}>
                                            {item.dia_evento} <Text style={{  color: Theme.silver }}>{item.mes_evento.substring(0, 3).toUpperCase()}</Text>  {item.año_evento}
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </ScrollView>
                    </View>

                </ScrollView>
            </View >
        );
    }
}

export default EventDetailsScreen;

const styles = StyleSheet.create({
    branchContainer: {
        flex: 1,
        paddingTop: 20,
        alignItems: "center",
        marginTop: 50,
        justifyContent: "center"
    },
    button: {
        width: "90%",
        height: 40,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        backgroundColor: Theme.darkGrey
    },
    buttonOther: {

        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
        backgroundColor: '#333333'
    },
    map: {
        height: 300,
        marginTop: 50
    },
    title: {
        fontSize: 25,
        marginTop: 20,
        marginLeft: 15,
        marginBottom: 15,
        color: Theme.darkGrey,
    },
});

//This is an example code for NavigationDrawer//
import React, { Component } from 'react';
//import react in our code.
import { ActivityIndicator, StatusBar,SafeAreaView, StyleSheet, View, Image, Text } from "react-native";
import { SearchBar } from 'react-native-elements';
import PlacesListView from '../components/PlacesListView';
import Theme from "../styles/Theme";
export default class Lugares extends Component {
  static navigationOptions = {  
    headerTitle: (
          <View style={{flex:1,}}>
              <Image
                  source={require('../img/logo.png') }
                  style={{marginLeft:60,width:120, height:40,marginTop:-15}}
              />
          </View>
      ),
      headerStyle: {  
          backgroundColor: '#ffffff',  
      },  
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      search: '',
      events: [],
      totalEvents: [],
    };

    this.eventsHolder = [];
    this.totalEventsHolder = [];
  };

  componentDidMount = () => {
    this.getEventList();
  };

  getEventList() {
    fetch(Theme.webservice_main_link+'eventos/')
      .then((response) => response.json())
      .then((responseJson) => {

        let array = responseJson.eventos

        const placesMap = Object.assign({}, ...array.map(s => ({ [s.lugar]: s.lugar })));
        const placesArray = Object.values(placesMap)

        // let PlacesEvents = [];
        // const test = responseJson.eventos.map((item, key) => {
        //   alert(key)
        //   let map = { "place": item.lugar, "events": [item] }
        //   PlacesEvents.push(map);
        // });

        this.setState({
          isLoading: false,
          events: placesArray,
          totalEvents: responseJson.eventos,
        }, function () {
          this.eventsHolder = array;
          this.totalEventsHolder = responseJson.eventos;
        });
      })
      .catch((error) => {
        console.error(error);
      });
  };


  search = text => {
  };

  clear = () => {
    this.search.clear();
  };

  searchFilterFunction(text) {
    //passing the inserted text in textinput
    let newData = this.totalEventsHolder.filter(function (item) {
      //applying filter for the inserted text in search bar
      const itemData = item.titulo ? item.titulo.toUpperCase() : ''.toUpperCase()
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });


    const newDataLugar = this.totalEventsHolder.filter(function (item) {
      //applying filter for the inserted text in search bar
      const itemData = item.lugar ? item.lugar.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    newData = newData.concat(
      newDataLugar.filter((object) => !newData.map((x) => x.id).includes(object.uniqueId)),
    );

    let array = newData

    const placesMap = Object.assign({}, ...array.map(s => ({ [s.lugar]: s.lugar })));
    const placesArray = Object.values(placesMap)

    this.setState({
      //setting the filtered newData on datasource
      //After setting the data it will automatically re-render the view
      events: placesArray,
      search: text,
    });
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20, justifyContent: 'center' }}>
          <ActivityIndicator size="large" color="#BD1624" />
        </View>
      )
    }

    const { search } = this.state.events;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      <View>
        {/* <StatusBar backgroundColor="blue" barStyle="light-content" /> */}
        <View style={{ width: "100%", height: "100%" }}>
          <SearchBar
            round
            searchIcon={{ size: 24,color:'#000' }}
            containerStyle={{ backgroundColor: "#FFFFFF" }}
            onChangeText={text => this.searchFilterFunction(text)}
            onClear={text => this.searchFilterFunction('')}
            placeholder="Buscar lugar..."
            value={this.state.search}
            placeholderTextColor={'#000000'}
            lightTheme
          />
          <PlacesListView
            itemList={this.state.events}
            totalItemList={this.state.totalEvents}
            props={this.props}
          />
        </View>
      </View>
      </SafeAreaView>

    );
  }
}
const styles = StyleSheet.create({
  branchContainer: {
    flex: 1,
    paddingTop: 20,
    alignItems: "center",
    marginTop: 50,
    justifyContent: "center"
  }
});


